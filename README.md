# frameloss skel files #

These are my base unix settings for a new account and is a mix of MacOS, Debian, RHEL, and BSD settings. There isn't anything private here, so I went ahead and made it public.

To use it as defaults, it would be easy to do something like . . . 

```
cd /etc
mv skel skel.orig
git clone --depth=1 https://bitbucket.org/frameloss/skel.git
rm -fr ./skel/README.md ./skel/.git/
```