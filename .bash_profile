# # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
# bash environment settings
# suitable for either .bash_profile or .bashrc
#
# tag@frameloss.org
# maintained at: https://bitbucket.org/frameloss/skel.git
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # #

shopt -s cdspell checkhash checkwinsize cmdhist extglob histappend progcomp 

# Setup paths for various *nix flavors:
pp=/bin
while read p; do
	[ -d "$p" ] && pp=$pp:$p
done << EOF
	/sbin
	/usr/bin
	/usr/sbin
	/usr/local/bin
	/usr/local/sbin
	/opt/bin
	/opt/sbin
	/opt/local/bin
	/opt/local/sbin
	$HOME/bin
	/opt/X11/bin
	/opt/aws/bin
	/usr/kerberos/bin
	/usr/kerberos/sbin
	/opt/logstash/bin
	/opt/logstash-forwarder/bin
	/opt/splunk/bin
	/opt/splunkforwarder/bin
	/opt/microsoft/scep/sbin
	/opt/google/chrome
EOF
# little paranoia here, if this didn't work we might be in a chroot, or recovery disk
#	don't overwrite PATH if we didn't get anything.
[ "$pp" == "/bin" ] || export PATH=$pp

# Super-duper space-saving, non-forking, color-changing PS1 (that doesn't mess up readline!)
export PS1='[\[\e[$(((($?>0))*31))m\]\h:\W\[\e[00m\]]\$ '

export HISTFILESIZE=1000
export HISTCONTROL=ignoredups:ignorespace
alias h="history 100"

###
###  FIX COLORS
###

PERL=false
which perl>/dev/null 2>&1 && PERL=true

UNAME=`uname`
if [ $UNAME = "Linux" ]; then
	LS_COLORS=""
	# dircolors gets upset without a TERM.  Not set? Guess.
	[ -z $TERM ] && export TERM=xterm-256color

	# Colored output is great, blue-colored directories are not.  Make them light grey.
	#	of course this is only really nice if you have a white on black terminal.
	# if we have perl ...
	#	do a quick regex against dircolors
	[ $PERL = "true" ] && which dircolors>/dev/null 2>&1 && eval `dircolors | perl -pe 's/(.*?)di=..;..:(.*)/\1di=00;37:\2/'`
	# If that didn't work, then try this.  This breaks on RHEL5, so check before setting it.
	[[ -z $LS_COLORS  && `grep -qsv "release 5" /etc/redhat-release >/dev/null 2>&1` ]] && {
		LS_COLORS='rs=0:di=00;37:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:'
		LS_COLORS="$LS_COLORS"'bd=40;33;01:cd=40;33;01:or=40;31;01:su=37;41:'
		LS_COLORS="$LS_COLORS"'sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:'
		LS_COLORS="$LS_COLORS"'ex=01;32:*.tar=01;31:*.tgz=01;31:*.arj=01;31:'
		LS_COLORS="$LS_COLORS"'*.taz=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:'
		LS_COLORS="$LS_COLORS"'*.txz=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:'
		LS_COLORS="$LS_COLORS"'*.dz=01;31:*.gz=01;31:*.lz=01;31:*.xz=01;31:'
		LS_COLORS="$LS_COLORS"'*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:'
		LS_COLORS="$LS_COLORS"'*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:'
		LS_COLORS="$LS_COLORS"'*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:'
		LS_COLORS="$LS_COLORS"'*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:'
		LS_COLORS="$LS_COLORS"'*.rz=01;31:*.jpg=01;35:*.jpeg=01;35:*.gif=01;35:'
		LS_COLORS="$LS_COLORS"'*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:'
		LS_COLORS="$LS_COLORS"'*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:'
		LS_COLORS="$LS_COLORS"'*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:'
		LS_COLORS="$LS_COLORS"'*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:'
		LS_COLORS="$LS_COLORS"'*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:'
		LS_COLORS="$LS_COLORS"'*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:'
		LS_COLORS="$LS_COLORS"'*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:'
		LS_COLORS="$LS_COLORS"'*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:'
		LS_COLORS="$LS_COLORS"'*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:'
		LS_COLORS="$LS_COLORS"'*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:'
		LS_COLORS="$LS_COLORS"'*.cgm=01;35:*.emf=01;35:*.axv=01;35:*.anx=01;35:'
		LS_COLORS="$LS_COLORS"'*.ogv=01;35:*.ogx=01;35:*.aac=00;36:*.au=00;36:'
		LS_COLORS="$LS_COLORS"'*.flac=00;36:*.mid=00;36:*.midi=00;36:'
		LS_COLORS="$LS_COLORS"'*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:'
		LS_COLORS="$LS_COLORS"'*.ra=00;36:*.wav=00;36:*.axa=00;36:*.oga=00;36:'
		LS_COLORS="$LS_COLORS"'*.spx=00;36:*.xspf=00;36';
		export LS_COLORS;
	}

	alias ls="ls -aFh --color=auto"
	alias lz="ls -laZ"
	alias pz="ps auxfcZ"
	alias pps="ps auxfc"

elif [ $UNAME = "Darwin" ]; then
	# Of course macos is totally different, this is close to the above.
	export LSCOLORS="hxfxcxdxbxegedabagacad"
	alias ls="ls -aFGh"
	alias lz="ls -la@O"
	alias pps="ps auxch"
	alias pps="ps auxch" 
	# Still need to figure out how to list sandboxes!
	alias pz="ps" #FIXME
# Everything else ... no color.
else
	alias ls="ls -aF"
fi

# My common typos ...
alias cdd="cd"
alias gerp="grep"
alias gep="grep"
alias ipconfig="ifconfig"
alias exut="exit"

# Stuff I forget to sudo for . . .
[ $USER == "root" ] || {
	alias apt="sudo apt";
	alias dpkg="sudo dpkg";
	alias apt-get="sudo apt-get";
	alias dpkg-reconfigure="sudo dpkg-reconfigure";
	alias yum="sudo yum";
	alias pkg="sudo pkg";
	alias port="sudo port";
	alias virsh="sudo virsh";
}

# vi(m) rules! Setup and alias, and some sane defaults if there isn't already a .vimrc
which vim>/dev/null 2>&1 && alias vi="vim" 
[ -f ~/.vimrc ] || echo -e "set mouse=a\nsyntax on\ncolorscheme desert\nset backupdir=~/.tmp//\nset directory=~/.tmp//" > ~/.vimrc

export EDITOR=vi # for various commands like visudo vigr and so on.
set -o vi # use vi for readline.

# I also like vim's syntax coloring pager.
[ -d /usr/share/vim ] && alias less=$(find /usr/share/vim -name 'less.sh' |sort -rn |head -1)

# grc is a really cool colorizer, if it's around use it with tail
which grc >/dev/null 2>&1 && alias tail="grc tail"

# will need this on MacOS to get docbook playing nice with asciidoc under macports.
[ -f /opt/local/etc/xml/catalog ] && export XML_CATALOG_FILES="/opt/local/etc/xml/catalog"

# It's a neat idea, but I hate waiting for some script to search my 
#	dpkg/rpm database EVERY TIME I typo. (which I do a lot.)
unset -f command_not_found_handle

# Private TMPDIR ...
if [ ! -d ~/.tmp ]; then
	mkdir ~/.tmp && chmod 0700 ~/.tmp
fi
export TMPDIR=~/.tmp

### ssh:
# Always use compression.
SSH_OPTS="-C"
[ -n "$DISPLAY" ]        || SSH_OPTS="${SSH_OPTS} -X"
# Start an agent (but be kinda smart about it.)
if [[ ! -S ${SSH_AUTH_SOCK} ]]; then 
	[ -f ~/.tmp/.ssh-agentrc ] && . ~/.tmp/.ssh-agentrc > /dev/null 2>&1
	TEMPSTRING=`uuidgen` >/dev/null 2>&1 || TEMPSTRING=`date "+%u"`
	ps -p $SSH_AGENT_PID >/dev/null 2>&1 && [ -r ${SSH_AUTH_SOCK} ] || ssh-agent -s -a ~/.tmp/.ssh_agent_socket.${TEMPSTRING} > ~/.tmp/.ssh-agentrc && . ~/.tmp/.ssh-agentrc > /dev/null # start an agent if we don't have one.
	unset TEMPSTRING
fi
alias ssh="ssh ${SSH_OPTS}"

# Setup a GPG agent for Yubikey GPG -> SSH bridge.
GPG_TTY=$(tty)
export GPG_TTY
if [ -f "${HOME}/.gpg-agent-info" ]; then
    . "${HOME}/.gpg-agent-info"
    export GPG_AGENT_INFO
    export SSH_AUTH_SOCK
fi

### do I have ack-grep?
which ack-grep >/dev/null 2>&1 && alias grep="ack-grep" 

# Stuff we only want to do for interactive login shells ...
#	WARNING!!! This is used by both bash_profile and bashrc
# 	if anything is echoed to STDOUT when run as .bashrc it will break scp.
#
if [ `basename $BASH_SOURCE` = ".bash_profile" ]; then
	# Print something important looking 
	echo
	uname -a 
	echo
	w
	echo
	df -lh |awk '/[0-9]%/ {print $(NF-1),"\t",$NF}' |grep -v aufs 2>/dev/null
	echo -ne "\n\tCWD: "
	pwd
	# If ansible was checked out from git in ~/bin, source the env file
	[ -f ~/bin/ansible/hacking/env-setup ] && . ~/bin/ansible/hacking/env-setup
fi

    